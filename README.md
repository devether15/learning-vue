# Learning-Vue.js

_Notes and exercices "hands on" from Gilbert Arévalo_ 

## Udemy 🚀

* **course**: Build Web Apps with Vue JS 2 & Firebase
* **Instructor**: Shaun Pelling
* **available in**: https://www.udemy.com/build-web-apps-with-vuejs-firebase/learn/v4/overview

*"What separates design from art is that design is meant to be... functional. ― Cameron Moll "*