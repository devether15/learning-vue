new Vue({
    el: '#app',
    data: {
        title: 'becoming a vue ninja',
        cars: ['Mazda','Honda','Toyota','Mitsubishi','Subaru'],
        ninjas: [
            { name: 'Jackie', age: 25, belt: 'orange'},
            { name: 'Lee', age: 30, belt: 'brown'},
            { name: 'Chuck', age: 35, belt: 'black'}
        ] 
    },
    methods: {
              
    }   
})