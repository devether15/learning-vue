new Vue({
    el: '#app',
    data: {
        title: 'becoming a vue ninja',
        showName: false,
        showAge: false   
    },
    methods: {
        toggleName(){
            this.showName = !this.showName 
        },
        toggleAge(){
            this.showAge = !this.showAge 
        }       
    }   
})