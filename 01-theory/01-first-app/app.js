new Vue({
    el: '#app',
    data: {
        title: 'becoming a vue ninja',
        name: "Bob"
    },
    methods: {
        greeting(time){            
            return `What's up, good ${time}, ${this.name}`
        }
    }
})
