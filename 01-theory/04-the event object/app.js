new Vue({
    el: '#app',
    data: {
        title: 'becoming a vue ninja',
        coordinates: {
            x: 0,
            y: 0
        }
        
    },
    methods: {
       logEvent(e){
           console.log(e);
       },
       logCoordinates(e){
           this.coordinates.x = e.offsetX
           this.coordinates.y = e.offsetY
       }
    }
})