new Vue({
    el: '#app',
    data: {
        title: 'becoming a vue ninja',
        wage: 10
    },
    methods: {
      changeWage(amount){
          this.wage += amount
      }
    }
})