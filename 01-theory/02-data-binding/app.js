new Vue({
    el: '#app',
    data: {
        title: 'becoming a vue ninja',
        name: "Bob",
        url: 'http://www.youtube.com',
        classes: ['one', 'two']
    },
    methods: {
        greeting(time){            
            return `What's up, good ${time}, ${this.name}`
        }
    }
})
