import firebase from 'firebase'
// import firestore from 'firebase/firestore'

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyCyEgpYTxmB0PMBJ6pmnlu9-FHgiMKjcqk",
    authDomain: "ninja-smoothies-5fcaf.firebaseapp.com",
    databaseURL: "https://ninja-smoothies-5fcaf.firebaseio.com",
    projectId: "ninja-smoothies-5fcaf",
    storageBucket: "ninja-smoothies-5fcaf.appspot.com",
    messagingSenderId: "1067038486797",
    appId: "1:1067038486797:web:c75b677b11db03fc750044"
  };
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  /* firebaseApp.firestore().settings({ timestampsInSnapshots: true }) */

  //export firestore database
  export default firebaseApp.firestore()