 import firebase from 'firebase'
 import firestore from 'firebase/firestore'
 
 // Your web app's Firebase configuration
 var firebaseConfig = {
    apiKey: "AIzaSyBE3dmTFsus_pk69i8BYBTRD_KZUAMy31Y",
    authDomain: "ninja-chat-2dd61.firebaseapp.com",
    databaseURL: "https://ninja-chat-2dd61.firebaseio.com",
    projectId: "ninja-chat-2dd61",
    storageBucket: "ninja-chat-2dd61.appspot.com",
    messagingSenderId: "880816882797",
    appId: "1:880816882797:web:a400c2fb38dcbd2f2feb64"
  };
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);  

  export default firebaseApp.firestore()